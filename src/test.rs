use super::Bitboard;

#[test]
fn test_vertical() {
    for i in 0..64 {
        assert_eq!((1 << i).vertical_flip(), 1 << (56 ^ i));
    }
}
#[test]
fn test_horizontal() {
    for i in 0..64 {
        assert_eq!((1 << i).horizontal_flip(), 1 << (7 ^ i));
    }
}
#[test]
fn test_diagonal() {
    for i in 0..64 {
        let j = (i >> 3 | i << 3) & 63;
        assert_eq!((1 << i).diagonal_flip(), 1 << j);
    }
}
#[test]
fn test_antidiagonal() {
    for i in 0..64 {
        let j = (i >> 3 | i << 3) & 63 ^ 63;
        assert_eq!((1 << i).antidiagonal_flip(), 1 << j);
    }
}
