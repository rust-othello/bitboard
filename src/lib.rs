pub trait Bitboard {
    fn vertical_flip(&self) -> Self;
    fn horizontal_flip(&self) -> Self;
    fn diagonal_flip(&self) -> Self;
    fn antidiagonal_flip(&self) -> Self;
    fn bit_reverse(&self) -> Self
    where
        Self: Sized,
    {
        self.vertical_flip().horizontal_flip()
    }
    ///the below two operation differs from chessprogramming wiki's
    ///the divergence comes from difference of encoding and the definition of main diagonal
    ///main diagonal as in matrix, and the rightmost bits are the lowest
    fn rotate_clockwise(&self) -> Self
    where
        Self: Sized,
    {
        self.vertical_flip().diagonal_flip()
    }
    fn rotate_anticlockwise(&self) -> Self
    where
        Self: Sized,
    {
        self.diagonal_flip().vertical_flip()
    }
}

impl Bitboard for u64 {
    fn vertical_flip(&self) -> Self {
        self.swap_bytes()
    }
    fn horizontal_flip(&self) -> Self {
        //lookup tables are faster
        //https://stackoverflow.com/questions/746171/efficient-algorithm-for-bit-reversal-from-msb-lsb-to-lsb-msb-in-c
        //but it occupies 256B of L1 cache
        //ARM has rbits
        let x = *self;
        let k = !0 / 3;
        let x = (x >> 1 & k) + ((x & k) << 1);
        let k = !0 / 5;
        let x = (x >> 2 & k) + ((x & k) << 2);
        let k = !0 / 17;
        let x = (x >> 4 & k) + ((x & k) << 4);
        x
    }
    fn diagonal_flip(&self) -> Self {
        let x = *self;
        let k = 0x00AA_00AA_00AA_00AA;
        let t = k & (x ^ x >> 07);
        let x = x ^ (t ^ t << 07);
        let k = 0x0000_CCCC_0000_CCCC;
        let t = k & (x ^ x >> 14);
        let x = x ^ (t ^ t << 14);
        let k = 0x0000_0000_F0F0_F0F0;
        let t = k & (x ^ x >> 28);
        let x = x ^ (t ^ t << 28);
        x
    }
    fn antidiagonal_flip(&self) -> Self {
        let x = *self;
        let k = 0x0055_0055_0055_0055;
        let t = k & (x ^ x >> 09);
        let x = x ^ (t ^ t << 09);
        let k = 0x0000_3333_0000_3333;
        let t = k & (x ^ x >> 18);
        let x = x ^ (t ^ t << 18);
        let k = 0x0000_0000_0F0F_0F0F;
        let t = k & (x ^ x >> 36);
        let x = x ^ (t ^ t << 36);
        x
    }
}

pub struct PrintBoard<T>(pub T);
impl std::fmt::Debug for PrintBoard<u64> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        /*
        write!(
            f,
            "{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}\n{:08b}",
            self.0 & 0xFF,
            self.0 >> 8 & 0xFF,
            self.0 >> 16 & 0xFF,
            self.0 >> 24 & 0xFF,
            self.0 >> 32 & 0xFF,
            self.0 >> 40 & 0xFF,
            self.0 >> 48 & 0xFF,
            self.0 >> 56 & 0xFF
        )*/

        let mut temp = self.0;
        for _ in 0..8 {
            if let Err(e) = writeln!(f, "{:08b}", temp & 0xFF) {
                return Err(e);
            }
            temp >>= 8;
        }
        Ok(())
    }
}
mod test;
